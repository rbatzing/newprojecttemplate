# New Project Template

<img src="https://gitlab.com/rbatzing/newprojecttemplate/raw/master/process.png" alt="Project Development Logo" align="right"/> 
This GitLab repository provides a template for new CS499 projects at Payap University
Faculty of Science Department of Computer Science.
The purpose of this project is to distribute a template that organizes the project development space
and helps students manage the design,
development, testing and distribution of the products of their CS499 Senior Project.
This template borrows many concepts from the ISO29110 standard for sofware development
in which the student plays the role of the developing agency and the faculty project
advisor acts as the project client. However, whenever feesible and possible students
should also identify members of the target for which the product is being design to
also join in the review and testing the project design concepts and implementation.

----

**For More information: look at the documentation**
 